#!/bin/bash
FILEPATH=$1

PROGRAM=$(arm-none-eabi-size ${FILEPATH} | tail -n 1 | awk '{print $1}')
FLASH=32000
PERCENTS=$(arm-none-eabi-size ${FILEPATH} | tail -n 1 | awk '{print $1/32000*100}')

echo "${PROGRAM} out of ${FLASH} [B] = ${PERCENTS}%"
